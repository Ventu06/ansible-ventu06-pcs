#!/bin/bash

uptime | awk 'BEGIN {RS=","} /load/ {print "L: " $3}'
