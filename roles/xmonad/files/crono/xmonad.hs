import System.Exit
import System.IO
import XMonad
import XMonad.Actions.WorkspaceNames
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.NoBorders
import XMonad.Layout.Tabbed
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Util.EZConfig
import qualified XMonad.StackSet as W

--

main = xmonad =<< myBarConfig myConfig

--

myBarConfig = statusBar myBar myPP myToggleBarKey

myBar = "xmobar /home/ventu06/.xmonad/xmobarrc.hs"

myPP = xmobarPP
	{ ppCurrent = xmobarColor "#ffff00" "" . wrap "[" "]"
	, ppTitle   = xmobarColor "#00ff00" "" . shorten 75
	, ppVisible = wrap "(" ")"
	, ppUrgent  = xmobarColor "#ff0000" "#ffff00"
	, ppLayout =
		(\x -> case x of
			"Tall" -> "[T]"
			"Mirror Tall" -> "[M]"
			"Tabbed Simplest" -> "[S]"
			"Full" -> "[F]"
			_ -> x
		)
	}

myToggleBarKey XConfig {XMonad.modMask = modMask} = (modMask, xK_b)

--

myConfig = ewmh def
	{ terminal = myTerminal
	, borderWidth = myBorderWidth
--	, normalBorderColor = myNormalBorderColor
--	, focusedBorderColor = myFocusedBorderColor
--	, focusFollowsMouse = myFocusFollowsMouse
--	, clickJustFocuses
	, modMask = myModMask
--	, workspaces = myWorkspaces

--	, clientMask
--	, rootMask
--	, handleExtraArgs

	, keys = myKeys
--	, mouseBindings = myMouseBindings

	, layoutHook = myLayoutHook
--	, manageHook = myManageHook
--	, handleEventHook = myEventHook
--	, logHook = myLogHook
--	, startupHook = myStartupHook
	}

myTerminal = "lxterminal"

myBorderWidth = 2
myModMask = mod4Mask
--myWorkspaces = ["1","2","3","4","5","6","7","8","9"]

myKeys conf = mkKeymap conf $
	[ ("M-S-q", kill)
	, ("M-w", sendMessage NextLayout)
	, ("M-e", setLayout $ XMonad.layoutHook conf)
	, ("M-n", refresh)
	, ("M-<R>", windows W.focusDown)
	, ("M-<L>", windows W.focusUp)
	, ("M-<U>", windows W.focusMaster)
	, ("M-S-<R>", windows W.swapDown)
	, ("M-S-<L>", windows W.swapUp)
	, ("M-S-<U>", windows W.swapMaster)
	, ("M-è", sendMessage Shrink)
	, ("M-+", sendMessage Expand)
	, ("M-f", (sendMessage $ Toggle NBFULL) <+> (sendMessage ToggleStruts))
	, ("M-t", withFocused $ windows . W.sink)
	, ("M-,", sendMessage (IncMasterN 1))
	, ("M-.", sendMessage (IncMasterN (-1)))
	, ("M-S-e", io (exitWith ExitSuccess))
	] ++
	[("M-" ++ m ++ k, windows $ f i)
		| (i, k) <- zip (XMonad.workspaces conf) $ show <$> [1..9]
		, (f, m) <- [(W.greedyView, ""), (W.shift, "S-")]
	] ++
	[("M-" ++ m ++ k, screenWorkspace i >>= flip whenJust (windows . f))
		| (i, k) <- zip [0,1] ["o","p"]
		, (f, m) <- [(W.view, ""), (W.shift, "S-")]
	] ++
	[ ("M-<Return>", spawn myTerminal)
	, ("M-S-r", spawn
		"if type xmonad; \
		\ then xmonad --recompile \
		\ && killall xmobar \
		\ && xmonad --restart; \
		\ else xmessage xmonad not in \\$PATH: \"$PATH\"; \
		\ fi")
	, ("M-l", spawn "xscreensaver-command -lock")
	, ("M-<Tab>", spawn "rofi -show window")
	, ("M-d", spawn "rofi -show run")
	, ("M-S-d", spawn "clipmenu")
	, ("M-S-C-d", spawn "clipdel -d \".*\"")
	, ("M-c", spawn "rofi-pass")
	, ("M-S-c", spawn "rofi -show calc")
	, ("M-C-c", spawn "rofi -show emoji")
	, ("M-<KP_End>", spawn "pcmanfm-qt")
	, ("M-S-<KP_End>", spawnTerm "ranger")
	, ("M-<KP_Down>", spawn "firefox")
	, ("M-<KP_Page_Down>", spawn "thunderbird")
	, ("M-<KP_Left>", spawnTerm "htop")
	, ("M-<KP_Right>", spawn "telegram-desktop")
	, ("M-<KP_Subtract>", spawn "amixer set Master 5%+")
	, ("M-<KP_Divide>", spawn "amixer set Master 5%-")
	, ("M-<KP_Multiply>", spawn "amixer set Master toggle")
	, ("M-k", spawnTerm "/home/ventu06/scripts/asciiquarium_xmonad.sh")
	, ("M-S-k", spawnTerm "/home/ventu06/scripts/cmatrix_xmonad.sh")
	, ("M-j", spawnTerm "curl v2.wttr.in/Pisa; read")
	]

myLayoutHook = avoidStruts . smartBorders $ mkToggle (NBFULL ?? EOT) $
	Tall 1 (10/100) (50/100) |||
	Mirror (Tall 1 (10/100) (50/100)) |||
	tabbed shrinkText myTabConfig
	where
		myTabConfig = def
			{ fontName = "xft:DejaVu Sans Mono:size=9:antialias=true"
			, activeBorderColor = "#00ffff"
			, activeTextColor = "#ffff00"
			, activeColor = "#000000"
			, inactiveBorderColor = "#0000ff"
			, inactiveTextColor = "#00ff00"
			, inactiveColor = "#000000"
			, urgentBorderColor = "#0000ff"
			, urgentTextColor = "#ff0000"
			, urgentColor = "#000000"
			}

myManageHook = composeAll
	[ manageDocks
	, isFullscreen --> doFullFloat
	, manageHook def
	]

--

spawnTerm cmd = spawn $ myTerminal ++ " -e \"" ++ cmd ++ "\""
