Config

	{ font = "DejaVu Sans Mono 9"
	, bgColor = "#000000"
	, fgColor = "#00ff00"
	, position = BottomH 20
	, textOffset = 1
	, border = TopBM 1
	, borderWidth = 2
	, borderColor = "#0000ff"


	, sepChar = "%"
	, alignSep = "}{"
	, template = "%StdinReader%}{| %multicoretemp% | %load% | %memory% | %disku% | %ip% | %alsa:default:Master% | %date% "

	, lowerOnStart = True
	, hideOnStart = False
	, allDesktops = True
	, overrideRedirect = True
	, pickBroadest = False
	, persistent = True

	, commands =
		[ Run Alsa "default" "Master"
			[ "-t", "V: <volume>% <status>"
			, "-p", "3"
			, "--"
			, "-o", "<fc=#ffff00>[X]</fc>"
			, "-O", "<fc=#00ff00>[O]</fc>"
			]
		, Run Com "/home/ventu06/.xmonad/ip.sh" [] "ip" 50
		, Run Com "/home/ventu06/.xmonad/load.sh" [] "load" 50
		, Run Date "%d-%m-%Y (%a) - %T" "date" 10
		, Run DiskU
			[ ("/", "/: <free>")
			, ("/home/ventu06", "| ~: <free>")
			, ("/run/mount/Data_1", "| D1: <free>")
			, ("/run/mount/Data_2", "| D2: <free>")
			]
			[ "-L", "20"
			, "-H", "40"
			, "-l", "#ff0000"
			, "-n", "#ffff00"
			, "-h", "#00ff00"
			] 50
		, Run Memory
			[ "-t", "M: <usedratio>%"
			, "-L", "60"
			, "-H", "80"
			, "-l", "#00ff00"
			, "-n", "#ffff00"
			, "-h", "#ff0000"
			, "-p", "3"
			] 10
		, Run MultiCoreTemp
			[ "-t", "T: <avg>°C"
			, "-L", "60"
			, "-H", "80"
			, "-l", "#00ff00"
			, "-n", "#ffff00"
			, "-h", "#ff0000"
			, "-p", "2"
			] 50
		, Run StdinReader
		]
	}
