run:
	ansible-playbook --diff -i hosts playbook.yml

check:
	ansible-playbook --diff --check -i hosts playbook.yml

install:
	ansible-galaxy collection install -r requirements.yml --upgrade
